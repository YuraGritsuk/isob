# -*- coding: utf-8 -*-

from collections import deque
import string


russian_letters_lowercase = 'абвгдеёжзийклмнопрстуфхцчшщъыьэюя'
alphabet = (string.ascii_letters + russian_letters_lowercase + russian_letters_lowercase.upper() +
            string.digits + string.punctuation)


def encrypt(text, encrypt_mapping):
    return ''.join(encrypt_mapping[symbol] for symbol in text if symbol in encrypt_mapping)


def decrypt(encrypted_text, decrypt_mapping):
    return ''.join(decrypt_mapping[symbol] for symbol in encrypted_text if symbol in decrypt_mapping)


def shift_alphabet(alphabet, shift):
    deq = deque(alphabet)
    deq.rotate(-shift)
    return ''.join(symbol for symbol in deq)


if __name__ == '__main__':
    shift = 1
    shifted_alphabet = shift_alphabet(alphabet, shift)

    with open('input_1.txt', 'r', encoding='utf-8') as input_file:
        text = input_file.read()

    encrypt_mapping = {key: value for key, value in zip(alphabet, shifted_alphabet)}
    encrypted_text = encrypt(text, encrypt_mapping)

    decrypt_mapping = {key: value for key, value in zip(shifted_alphabet, alphabet)}
    decrypted_text = decrypt(encrypted_text, decrypt_mapping)

    print('{0}-->{1}-->{2}'.format(text, encrypted_text, decrypted_text))
    with open('output_1.txt', 'w', encoding='utf-8') as output_file:
        output_file.write(encrypted_text)
