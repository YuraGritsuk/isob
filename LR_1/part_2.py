# -*- coding: utf-8 -*-

import string
from math import ceil
from itertools import repeat


russian_letters = 'абвгдеёжзийклмнопрстуфхцчшщъыьэюя'
alphabet = string.ascii_letters + string.digits + string.punctuation + russian_letters + russian_letters.upper()


def encrypt(text, key_word):
    encrypted_text = ''
    for index, symbol in enumerate(text):
        encrypted_text += alphabet[
            (alphabet.index(symbol) + alphabet.index(key_word[index])) % len(alphabet)
        ]
    return encrypted_text


def decrypt(encrypted_text, key_word):
    decrypted_text = ''
    for index, symbol in enumerate(encrypted_text):
        decrypted_text += alphabet[
            (alphabet.index(symbol) - alphabet.index(key_word[index])) % len(alphabet)
        ]
    return decrypted_text


def extend_key_word(input_text, key_word):
    return ''.join(repeat(key_word, ceil(len(input_text) / len(key_word))))


if __name__ == '__main__':
    with open('input_2.txt', 'r', encoding='utf-8') as input_file:
        text = input_file.read()
    key_word = extend_key_word(text, 'LEMON')

    encrypted_text = encrypt(text, key_word)
    decrypted_text = decrypt(encrypted_text, key_word)
    print('{0}-->{1}-->{2}'.format(text, encrypted_text, decrypted_text))

    with open('output_2.txt', 'w', encoding='utf-8') as output_file:
        output_file.write(encrypted_text)

