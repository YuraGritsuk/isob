#!/usr/bin/env python3.7

from functools import partial
import selectors
import socket
from threading import Thread
import tkinter
from queue import Queue


def handle_messages(sock, outcome_messages, income_messages_list):
    with selectors.DefaultSelector() as selector:
        selector.register(sock, selectors.EVENT_READ | selectors.EVENT_WRITE)

        client_alive = True
        while client_alive:
            events = selector.select(0.1)
            for key, mask in events:
                if mask & selectors.EVENT_READ:
                    message = sock.recv(2048)
                    if message:
                        income_messages_list.insert(tkinter.END, message.decode('utf-8'))
                if mask & selectors.EVENT_WRITE and not outcome_messages.empty():
                    message = outcome_messages.get()
                    sock.sendall(message.encode('utf-8'))
                    outcome_messages.task_done()
                    if message == '{quit}':
                        client_alive = False


def send(outcome_messages, input_field, top, event=None):
    message = input_field.get()
    input_field.set('')

    if message == '{quit}':
        quit_(outcome_messages, top)
    elif message != '':
        outcome_messages.put(message)


def quit_(outcome_messages, top):
    outcome_messages.put('{quit}')
    outcome_messages.join()
    top.quit()


def setup_tkinter():
    top = tkinter.Tk()
    top.title('Client')

    messages_frame = tkinter.Frame(top)

    scrollbar = tkinter.Scrollbar(messages_frame)
    scrollbar.pack(side=tkinter.RIGHT, fill=tkinter.Y)

    income_messages_list = tkinter.Listbox(messages_frame, height=15, width=60, yscrollcommand=scrollbar.set)
    income_messages_list.pack(side=tkinter.LEFT, fill=tkinter.BOTH)
    income_messages_list.pack()

    outcome_messages = Queue()

    messages_frame.pack()

    input_field = tkinter.StringVar()
    input_field.set("client")

    entry_field = tkinter.Entry(top, textvariable=input_field)
    entry_field.bind("<Return>", partial(send, outcome_messages, input_field, top))
    entry_field.pack()

    send_button = tkinter.Button(top, text="Send", command=partial(send, outcome_messages, input_field, top))
    send_button.pack()

    top.protocol("WM_DELETE_WINDOW", partial(quit_, outcome_messages, top))

    return income_messages_list, outcome_messages


if __name__ == '__main__':
    HOST = 'localhost'
    PORT = 8000

    with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as sock:
        sock.connect((HOST, PORT))
        sock.setblocking(False)
        income_messages_list, outcome_messages = setup_tkinter()
        Thread(target=handle_messages, args=(sock, outcome_messages, income_messages_list)).start()
        tkinter.mainloop()
