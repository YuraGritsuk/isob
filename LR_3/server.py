#!/usr/bin/env python3.7

from dataclasses import dataclass
import re
import socket
import socketserver
import time


@dataclass
class Client:
    sock: socket.socket
    address: tuple
    name: str


class UDPChatServer(socketserver.ThreadingUDPServer):

    def __init__(self, *args, **kwargs):
        self.clients = {}
        super().__init__(*args, **kwargs)


class UDPChatRequestHandler(socketserver.DatagramRequestHandler):

    KEYWORDS = {'{quit}'}
    WRONG_NAME_STR = "This name isn't allowed. Try again..."
    JOINED_CHAT_STR = 'joined the chat'
    LEFT_CHAT_STR = 'left the chat'

    def handle(self):
        data = self.request[0].decode('utf-8').strip()
        sock = self.request[1]
        address = self.client_address

        if address not in self.server.clients:
            name = data
            if not self.is_valid_name(name):
                # sock.sendto(self.WRONG_NAME_STR.encode('utf-8'), address)
                return
            self.server.clients[address] = Client(sock, address, name)
            # self.broadcast_message(self.JOINED_CHAT_STR, name)
        elif data in self.KEYWORDS:
            self.perform_action(data, self.server.clients[address])
        else:
            self.broadcast_message(data, self.server.clients[address].name)

    def broadcast_message(self, message, name):
        for client in self.server.clients.values():
            client.sock.sendto(
                time.strftime(f'%H:%M {name}: {message}', time.localtime()).encode('utf-8'), client.address
            )

    @staticmethod
    def is_valid_name(name):
        if re.fullmatch(r'[A-Za-z]\w{1,7}', name, re.ASCII):
            return True
        return False

    def perform_action(self, action: str, client):
        if action == '{quit}':
            self.disconnect_client(client)

    def disconnect_client(self, client):
        self.broadcast_message(self.LEFT_CHAT_STR, client.name)
        del self.server.clients[client.address]


if __name__ == '__main__':
    HOST, PORT = 'localhost', 8000
    with UDPChatServer((HOST, PORT), UDPChatRequestHandler) as server:
        server.serve_forever()
